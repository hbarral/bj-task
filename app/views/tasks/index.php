<?php require APP_ROOT . '/views/includes/header.php'; ?>
<div class="row row-alert justify-content-sm-center">
  <div id="myAlert" class="alert alert-dismissible fade show">
    <strong>Warning!</strong> <span> A simple warning alert</span>
    <button type="button" class="close">&times;</button>
  </div>
</div>

<div class="row">
  <div class="col-md-10 offset-1">
    <h1>Задачи</h1>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <form class="form-inline justify-content-center" id="create-task" data-parsley-errors-messages-disabled>
      <label class="sr-only" for="inlineFormInputGroupUsername2">Username</label>
      <div class="input-group mb-2 mr-sm-2">
        <div class="input-group-prepend">
          <div class="input-group-text">@</div>
        </div>
        <input
          name="username"
          type="text"
          class="form-control"
          id="inlineFormInputGroupUsername2"
          placeholder="имя пользователя"
          onfocus="this.placeholder=''"
          onblur="this.placeholder='имя пользователя'"
          required>
      </div>

      <label class="sr-only" for="inlineFormInputGroupEmail">Email</label>
      <div class="input-group mb-2 mr-sm-2">
        <div class="input-group-prepend">
          <div class="input-group-text"><i class="fas fa-envelope"></i></div>
        </div>
        <input
          name="email"
          type="email"
          class="form-control"
          id="inlineFormInputGroupEmail"
          placeholder="john@mail.ua"
          onfocus="this.placeholder=''"
          onblur="this.placeholder='john@mail.ua'"
          data-parsley-trigger="change"
          required
          >
      </div>

      <label class="sr-only" for="inlineFormInputTask">Task</label>
      <div class="input-group mb-2 mr-sm-2">
        <div class="input-group-prepend">
          <div class="input-group-text"><i class="fas fa-tasks"></i></div>
        </div>
        <input
          name="task"
          type="text"
          class="form-control"
          id="inlineFormInputTask"
          placeholder="купить молоко"
          onfocus="this.placeholder=''"
          onblur="this.placeholder='купить молоко'"
          required>
      </div>

      <button type="submit" class="btn btn-primary mb-2">Добавлять</button>
    </form>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <table id="grid"></table>
  </div>
</div>

<?php require APP_ROOT . '/views/includes/footer.php'; ?>
