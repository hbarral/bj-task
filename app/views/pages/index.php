<?php require APP_ROOT . '/views/includes/header.php'; ?>
  <h1><?php echo $data['title']; ?></h1>

<div class="jumbotron jumbotron-fluid text-center">
  <div class="container">
    <div class=""display-3""><?php echo $data['title']; ?></div>
    <p class="lead"><?php echo $data['description']; ?></p>
  </div>
</div>


<?php require APP_ROOT . '/views/includes/footer.php'; ?>
