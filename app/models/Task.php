<?php

class Task
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getTasks($page = 1, $limit = 3, $sortby = null, $direction = null)
    {
        $start = ($page - 1) * $limit;

        if ($sortby && $direction) {
            $sql = "SELECT * FROM tasks ORDER BY $sortby $direction LIMIT :start, :limit";
        } else {
            $sql = "SELECT * FROM tasks LIMIT :start, :limit";
        }

        $this->db->query($sql);
        $this->db->bind(':limit', intval($limit, 10), PDO::PARAM_INT);
        $this->db->bind(':start', intVal($start, 10), PDO::PARAM_INT);
        $results = $this->db->resultSet();

        return $results;
    }

    public function addTask($data)
    {
        $sql = 'INSERT INTO tasks (username, email, task) VALUES (:username, :email, :task)';
        $this->db->query($sql);
        $this->db->bind(':username', $data['username']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':task', $data['task']);

        return $this->db->execute() ? true : false;
    }

    public function update($data)
    {
        $sql = 'UPDATE tasks SET task = :task, status = :status, modified_by = :modified_by WHERE id = :id';
        $this->db->query($sql);
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':task', $data['task']);
        $this->db->bind(':status', $data['status']);
        $this->db->bind(':modified_by', $data['modified_by']);

        return ($this->db->execute()) ? true : false;
    }

    public function delete($id)
    {
        $sql = 'DELETE FROM tasks WHERE id = :id';
        $this->db->query($sql);
        $this->db->bind(':id', $id);

        return $this->db->execute() ? true : false;
    }

    public function getTotal()
    {
        $sql = 'SELECT COUNT(*) FROM tasks';
        $query = $sql;
        $this->db->query($query);
        $this->db->execute();
        $count = $this->db->count();

        return $count;
    }
}
