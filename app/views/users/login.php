<?php require APP_ROOT . '/views/includes/header.php'; ?>

<div class="row">
  <div class="col-md-6 mx-auto">
    <div class="card card-body bg-light mt-5">
      <?php flash('register_success'); ?>
      <h2>Авторизоваться</h2>
      <p>Пожалуйста, заполните свои учетные данные, чтобы войти</p>
      <form action="<?php echo URL_ROOT; ?>/users/login" method="post">

        <div class="form-group">
          <label for="email">Имя пользователя: <sup>*</sup></label>
          <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>">
          <span class="invalid-feedback"><?php echo $data['name_error']; ?></span>
        </div>

        <div class="form-group">
          <label for="password">Пароль: <sup>*</sup></label>
          <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>">
          <span class="invalid-feedback"><?php echo $data['password_error']; ?></span>
        </div>

        <div class="row">
          <div class="col">
         <input type="submit" value="Вход" class="btn btn-success btn-block">
          </div>
          <div class="col">
            <a href="<?php echo URL_ROOT; ?>/users/register" class="btn btn-light btn-block">Нет аккаунта? регистр</a>
          </div>
        </div>

      </form>


    </div>
  </div>
</div>

<?php require APP_ROOT . '/views/includes/footer.php'; ?>
