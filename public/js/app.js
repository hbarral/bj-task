$(document).ready(function(){
  function notify(data){
    const alert = $('#myAlert')
    const title = $('#myAlert > strong')
    const message = $('#myAlert span')

    title.text(data.title)
    message.text(data.message)

    alert.addClass(data.type)
    alert.show()

    setTimeout(() => {
      alert.hide()
      alert.removeClass(data.type)
    }, 2500)
  }

  async function addTask(data){
    const response = await fetch('/tasks/add', {
      method: 'post',
      body: data
    })

    const result = await response.json()

    if (result.success){
      notify({ type: 'alert-success', title: 'Успех!', message: `добавлена задача «${ result.message.task }»` })
      grid.reload()
    }
    return response
  }


  var grid;
  var managment = window.isAdmin ? true : false;

  grid = $('#grid').grid({
    dataSource: '/tasks/index',
    uiLibrary: 'bootstrap4',
    primaryKey: 'id',
    inlineEditing: { mode: 'command', managementColumn: managment },
    columns: [
      { field: 'id', title: 'Id', width: 44 },
      { field: 'username', title: 'Username', editor: false, sortable: true },
      { field: 'email', title: 'Email',  editor: false, sortable: true },
      { field: 'task', title: 'Task',  editor: true },
      { field: 'status', title: 'Completed?', type: 'checkbox', editor: true, width: 150, align: 'center', sortable: true },
      { field: 'modified_by', title: 'Modified By', editor: false }
    ],
    pager: { limit: 3 }
  });

  if (grid){

    grid.on('rowDataChanged', function (e, id, record) {
      $.ajax({ url: '/tasks/update', data: record , method: 'POST' })
        .done(function() {
          grid.reload()
          notify({ type: 'alert-info', title: 'Информация!', message: `задание с идентификатором «${ record.id }» было изменено` })
        })
        .fail(function () {
          alert('Failed to save.');
        })
    })

    grid.on('rowRemoving', function (e, $row, id, record) {
      if (confirm('Are you sure?')) {
        $.ajax({ url: '/tasks/delete', data: { id: id }, method: 'POST' })
          .done(function () {
            grid.reload()
            notify({ type: 'alert-danger', title: 'Внимание!', message: `задача «${ record.task }» была удалена` })
          })
          .fail(function () {
            alert('Failed to delete.')
          })
      }
    })
  }

  $('.close').click(function(){
    $('#myAlert').hide()
  });


  const validator = $('#create-task').parsley();
  const addTaskForm = document.getElementById('create-task');

  if( addTaskForm ){
    addTaskForm.addEventListener('submit', function(e){
      e.preventDefault()
      if (validator.isValid()){
        const formData = new FormData(addTaskForm);
        addTask(formData)
          .then(() => {
            addTaskForm.reset()
            validator.reset()
          })
      }
    })
  }

})
