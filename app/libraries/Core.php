<?php

class Core
{
    protected $currentController = 'Tasks';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {
        $url = $this->getUrl();

        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
            $this->currentController = ucwords($url[0]);
            unset($url[0]);
        }

        require_once '../app/controllers/' . $this->currentController . '.php';
        $this->currentController = new $this->currentController;

        if (isset($url[1])) {
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }

        $this->params = $url[2] ? $url[2] : [];

        call_user_func_array([$this->currentController, $this->currentMethod], [$this->params]);
    }

    public function getUrl()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $url = $_SERVER['REQUEST_URI'];
            $url = trim($url, '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);

            $path = parse_url($url, PHP_URL_PATH);
            $path = explode('/', $path);
            if (count($path) === 1) {
                $path[1] = 'index';
            }
            $params = parse_url($url, PHP_URL_QUERY);

            $paramsArray = [];

            if ($params) {
                parse_str($params, $paramsArray);
            }

            $path[2] = $paramsArray;

            return $path;
        }
    }
}
