<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">BJ Task</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/tasks/index">Задачи <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/tasks/about">О нас</a>
        </li>
      </ul>


      <ul class="navbar-nav">
        <?php if (isset($_SESSION['user_id'])): ?>
        <li class="nav-item">
          <a class="nav-link" href="/users/logout">Выйти<span class="sr-only">(current)</span></a>
        </li>

        <?php else : ?>
        <li class="nav-item">
          <a class="nav-link" href="/users/register">Зарегистрировать<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/users/login">Авторизоваться</a>
        </li>
        <?php endif; ?>
      </ul>

    </div>
  </div>
</nav>
