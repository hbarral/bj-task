<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo URL_ROOT; ?>/css/parsley.css">
    <link rel="stylesheet" href="<?php echo URL_ROOT; ?>/css/style.css">
    <title><?php echo SITE_NAME; ?></title>
  </head>
  <body>
    <?php require APP_ROOT . '/views/includes/navbar.php'; ?>
    <div class="container">
