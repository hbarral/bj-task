<?php

class Tasks extends Controller
{
    public function __construct()
    {
        $this->taskModel = $this->model('Task');
    }
    public function index($params = null)
    {
        $is_ajax = 'XMLHttpRequest' == ($_SERVER['HTTP_X_REQUESTED_WITH'] ?? '');
        if ($is_ajax) {
            $page = $params['page'];
            $limit = $params['limit'];
            $sortBy = $params['sortBy'] ?? null;
            $direction = $params['direction'] ?? null;

            $tasks = $this->taskModel->getTasks($page, $limit, $sortBy, $direction);
            $total = $this->taskModel->getTotal();

            foreach ($tasks as $task) {
                $task->status = $task->status ? true : false;
            }

            $data = [
              'records' => $tasks,
              'total' => $total
            ];

            echo json_encode($data);
        } else {
            $this->view('tasks/index');
        }
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
            'username' => trim($_POST['username']),
            'email' =>  trim($_POST['email']),
            'task' => trim($_POST['task']),

            'username_error' => '',
            'email_error' => '',
            'task_error' => ''
          ];

            if (empty($data['username'])) {
                $data['username_error'] = 'Пожалуйста, введите имя пользователя';
            }
            if (empty($data['email'])) {
                $data['email_error'] = 'Пожалуйста, введите адрес электронной почты';
            }
            if (empty($data['task'])) {
                $data['task_error'] = 'Пожалуйста, введите задачу';
            }

            if (empty($data['username_error']) && empty($data['email_error']) && empty($data['task_error'])) {
                if ($this->taskModel->addTask($data)) {
                    http_response_code(201);
                    echo json_encode([
                    'success' => true,
                    'message' => $data
                  ]);
                } else {
                    http_response_code(503);
                    echo json_encode([
                    'error' => 'Что-то пошло не так'
                  ]);
                }
            }
        } else {
            http_response_code(501);
            echo json_encode([
            'error' => 'Not Implemented'
          ]);
        }
    }

    public function delete()
    {
        if (!isAdmin()) {
            http_response_code(403);
            echo json_encode('Unauthorized');
            exit();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $id = $_POST['id'];
            if ($this->taskModel->delete($id)) {
                http_response_code(200);
                echo json_encode($id);
            } else {
                http_response_code(503);
                echo json_encode('error');
            }
        } else {
            redirect('tasks');
        }
    }

    public function update()
    {
        if (!isAdmin()) {
            http_response_code(403);
            echo json_encode('Unauthorized');
            exit();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
            'id' => trim($_POST['id']),
            'task' => trim($_POST['task']),
            'status' => (trim($_POST['status']) == 'true') ? 1 : 0,
            'modified_by' => EDITED_BY,

            'id_error' => '',
            'task_error' => ''
          ];

            if (empty($data['id'])) {
                $data['id_error'] = 'ID недействителен';
            }
            if (empty($data['task'])) {
                $data['task_error'] = 'Пожалуйста, введите задачу';
            }

            if (empty($data['id_error']) && empty($data['task_error'])) {
                if ($this->taskModel->update($data)) {
                    http_response_code(200);
                    echo json_encode([
                    'success' => true
                  ]);
                } else {
                    http_response_code(503);
                    echo json_encode([
                    'error' => 'Что-то пошло не так'
                  ]);
                }
            }
        } else {
            http_response_code(501);
            echo json_encode([
            'error' => 'Not Implemented'
          ]);
        }
    }

    public function about()
    {
        $data = [
        'title' => 'О нас',
          'description' => 'Приложение для создания задач'
      ];
        $this->view('tasks/about', $data);
    }
}
