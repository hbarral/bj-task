<?php

class Users extends Controller
{
    public function __construct()
    {
        $this->userModel = $this->model('User');
    }

    public function register()
    {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = [
            'name' => trim($_POST['name']),
            'email' => trim($_POST['email']),
            'password' => trim($_POST['password']),
            'confirm_password' => trim($_POST['confirm_password']),
            'name_error' => '',
            'email_error' => '',
            'password_error' => '',
            'confirm_password_error' => ''
          ];

            if (empty($data['email'])) {
                $data['email_error'] = 'Пожалуйста, введите адрес электронной почты';
            } else {
                if ($this->userModel->findUserByEmail($data['email'])) {
                    $data['email_error'] = 'E-mail уже занят';
                }
            }

            if (empty($data['name'])) {
                $data['name_error'] = 'Пожалуйста, введите имя';
            } else {
                if ($this->userModel->findUserByName($data['name'])) {
                    $data['name_error'] = 'Это пользователь уже существует';
                }
            }

            if (empty($data['password'])) {
                $data['password_error'] = 'Пожалуйста введите пароль';
            } elseif (strlen($data['password']) < 3) {
                $data['password_error'] = 'Пароль должен содержать не менее 3 символов';
            }

            if (empty($data['confirm_password'])) {
                $data['confirm_password_error'] = 'Пожалуйста, подтвердите пароль';
            } else {
                if ($data['password'] != $data['confirm_password']) {
                    $data['confirm_password_error'] = 'Пароль не подходит';
                }
            }

            if (
              empty($data['email_error']) &&
              empty($data['name_error']) &&
              empty($data['password_error']) &&
              empty($data['confirm_password_error'])
            ) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                if ($this->userModel->register($data)) {
                    flash('register_success', 'Вы зарегистрированы и можете войти');
                    redirect('users/login');
                } else {
                    die('Что-то пошло не так');
                }
            } else {
                $this->view('users/register', $data);
            }
        } else {
            $data = [
            'name' => '',
            'email' => '',
            'password' => '',
            'confirm_password' => '',
            'name_error' => '',
            'email_error' => '',
            'password_error' => '',
            'confirm_password_error' => ''
          ];

            $this->view('users/register', $data);
        }
    }

    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
            'name' => trim($_POST['name']),
            'password' => trim($_POST['password']),
            'name_error' => '',
            'password_error' => '',
          ];

            if (empty($data['name'])) {
                $data['name_error'] = 'Пожалуйста, введите адрес электронной почты';
            }

            if (empty($data['password'])) {
                $data['password_error'] = 'Пожалуйста введите пароль';
            }

            if (! $this->userModel->findUserByName($data['name'])) {
                $data['name_error'] = 'Произошла ошибка с вашими учетными данными';
                $data['password_error'] = 'Произошла ошибка с вашими учетными данными';
            }

            if (empty($data['name_error']) && empty($data['password_error'])) {
                $loggedInUser = $this->userModel->login($data['name'], $data['password']);
                if ($loggedInUser) {
                    $this->createUserSession($loggedInUser);
                } else {
                    $data['name_error'] = 'Произошла ошибка с вашими учетными данными';
                    $data['password_error'] = 'Произошла ошибка с вашими учетными данными';
                    $this->view('users/login', $data);
                }
            } else {
                $this->view('users/login', $data);
            }
        } else {
            $data = [
            'email' => '',
            'password' => '',
            'email_error' => '',
            'password_error' => '',
          ];
            $this->view('users/login', $data);
        }
    }

    public function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;

        redirect('tasks');
    }

    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();

        redirect('tasks');
    }
}
