<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function register($data)
    {
        $sql = 'INSERT INTO users(name, email, password) values(:name, :email, :password)';
        $this->db->query($sql);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        return ($this->db->execute()) ? true : false;
    }

    public function findUserByEmail($email)
    {
        $sql = 'SELECT * FROM users WHERE email = :email';
        $this->db->query($sql);
        $this->db->bind(':email', $email);
        $this->db->single();

        return ($this->db->rowCount() > 0) ? true : false;
    }

    public function findUserByName($name)
    {
        $sql = 'SELECT * FROM users WHERE name = :name';
        $this->db->query($sql);
        $this->db->bind(':name', $name);
        $this->db->single();

        return ($this->db->rowCount() > 0) ? true : false;
    }

    public function login($name, $password)
    {
        $sql = 'SELECT * FROM users WHERE name = :name';
        $this->db->query($sql);
        $this->db->bind(':name', $name);
        $row = $this->db->single();
        $hashed_password = $row->password;

        return (password_verify($password, $hashed_password)) ? $row : false;
    }
}
